// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ALaucher.generated.h"


class UStaticMeshComponent;
class UBoxComponent;

UCLASS()
class TEST_API AALaucher : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AALaucher();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		UBoxComponent* BoxComp;

	UPROPERTY(VisibleAnywhere, Category = "Effects")
		UParticleSystem* PickupFX;

	void PlayEffects();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

};
