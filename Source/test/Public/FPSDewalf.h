// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FPSDewalf.generated.h"

class UPawnSensingComponent;

UCLASS()
class TEST_API AFPSDewalf : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPSDewalf();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UPawnSensingComponent* PawnSensingComp;

	UFUNCTION()
	void OnPawnSeen(APawn* SeenPawn);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


};
