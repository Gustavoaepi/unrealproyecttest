// Fill out your copyright notice in the Description page of Project Settings.


#include "ALaucher.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"
#include <test\testCharacter.h>

// Sets default values
AALaucher::AALaucher()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RootComponent = MeshComp;
	BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	BoxComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	BoxComp->SetupAttachment(MeshComp);

}

// Called when the game starts or when spawned
void AALaucher::BeginPlay()
{
	Super::BeginPlay();
	
}

void AALaucher::PlayEffects()
{
	UGameplayStatics::SpawnEmitterAtLocation(this, PickupFX, GetActorLocation());
}

// Called every frame
void AALaucher::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AALaucher::NotifyActorBeginOverlap(AActor* OtherActor)
{

	Super::NotifyActorBeginOverlap(OtherActor);
	PlayEffects();

	UE_LOG(LogTemp, Log, TEXT("toco"));

	AtestCharacter* MyCharacterActor = Cast<AtestCharacter>(OtherActor);
	FVector vector = FVector(0, 0, 1500);
	if (MyCharacterActor)
	{
		UE_LOG(LogTemp, Log, TEXT("te lanse papa"));
		
		
		MyCharacterActor->LaunchCharacter(vector,false,false);
	}


}

