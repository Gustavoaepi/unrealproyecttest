// Fill out your copyright notice in the Description page of Project Settings.


#include "FPSExtractionZone.h"
#include "Components/BoxComponent.h"
#include "Components/DecalComponent.h"
#include "Kismet\GameplayStatics.h"
#include <test\testCharacter.h>
#include <test\testGameMode.h>



// Sets default values
AFPSExtractionZone::AFPSExtractionZone()
{

	const float VecDimention = 200.0f;

	OverlapComp = CreateDefaultSubobject<UBoxComponent>(TEXT("OverlapComp"));
	OverlapComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	OverlapComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	OverlapComp->SetBoxExtent(FVector(VecDimention));
	RootComponent = OverlapComp;

	// muestra las aristas del UBoxComponent cuando esta en false
	OverlapComp->SetHiddenInGame(true);

	OverlapComp->OnComponentBeginOverlap.AddDynamic(this, &AFPSExtractionZone::HandleOverlap);

	DecalComp = CreateDefaultSubobject<UDecalComponent>(TEXT("DecalComp"));
	DecalComp->DecalSize = FVector(VecDimention, VecDimention, VecDimention);
	DecalComp->SetupAttachment(RootComponent);
	
}


void AFPSExtractionZone::HandleOverlap(UPrimitiveComponent* OverlappedComponent,AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,bool bFromSweep, const FHitResult& SweepResult)
{


	AtestCharacter* MyCharacterActor = Cast<AtestCharacter>(OtherActor);

	if (MyCharacterActor == nullptr)
	{
		return;
	}

		UE_LOG(LogTemp, Log, TEXT("Entraste en la zona de extraccion"));

		if (MyCharacterActor->bIsCarryIngObject)
		{
			AtestGameMode* GM = Cast<AtestGameMode> (GetWorld()->GetAuthGameMode());

			if (GM)
			{
				GM->CompleteMission(MyCharacterActor);
			}
		}
		else
		{
			if (ObjectiveMissingSound)
			{
				UGameplayStatics::PlaySound2D(this, ObjectiveMissingSound);
			}
			
		}
}


