// Fill out your copyright notice in the Description page of Project Settings.


#include "FPSDewalf.h"
#include "Perception/PawnSensingComponent.h"
#include "DrawDebugHelpers.h"

// Sets default values
AFPSDewalf::AFPSDewalf()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));

	PawnSensingComp->OnSeePawn.AddDynamic(this, &AFPSDewalf::OnPawnSeen);

}

// Called when the game starts or when spawned
void AFPSDewalf::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFPSDewalf::OnPawnSeen(APawn* SeenPawn)
{
	if (SeenPawn == nullptr)
	{
		return;
	}

	UE_LOG(LogTemp, Log, TEXT("Entraste en la zona de extraccion"));
	DrawDebugSphere(GetWorld(), SeenPawn->GetActorLocation(),32.0f , 12, FColor::Yellow, false, 10.0f);
}

// Called every frame
void AFPSDewalf::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

